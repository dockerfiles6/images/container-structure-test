#!/usr/bin/env sh
export PRODUCT=container-structure-test
export SOURCE=GoogleContainerTools/container-structure-test

pip install lastversion --user

export VERSION=$(lastversion ${SOURCE})

docker build --build-arg CST_REF=v${VERSION} --build-arg VERSION=${VERSION} -t ${REGISTRY}/${PRODUCT}:${VERSION} .

