FROM golang:1.16.5-alpine3.12 AS builder
ARG CST_REF=v1.9.1
ENV SOURCE_PATH=/go/src/github.com/GoogleContainerTools/container-structure-test

RUN apk add --no-cache git~=2.26 make~=4.3 && \
  git clone --depth 1 https://github.com/GoogleContainerTools/container-structure-test.git --branch "$CST_REF" "$SOURCE_PATH"

WORKDIR $SOURCE_PATH
RUN VERSION=$(git describe --tags || echo "$CST_REF-$(git describe --always)") make && \
  chmod +x /go/src/github.com/GoogleContainerTools/container-structure-test/out/container-structure-test

# Distro image
FROM alpine:3.17
COPY --from=builder /go/src/github.com/GoogleContainerTools/container-structure-test/out/container-structure-test /bin/
ENTRYPOINT ["/bin/container-structure-test"]
